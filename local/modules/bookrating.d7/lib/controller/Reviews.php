<?php

namespace Bookrating\D7\Controller;

use Bitrix\Main\Engine\Controller;
use Bitrix\Main\Engine\ActionFilter;
use Bitrix\Main\Loader;
use Bitrix\Iblock\Elements\ElementReviewsTable;

class Reviews extends Controller
{
    public function configureActions()
    {
        return [
            'getList' => [
                'prefilters' => [
                    new ActionFilter\Authentication(),
                    new ActionFilter\HttpMethod(
                        [
                            ActionFilter\HttpMethod::METHOD_GET,
                            ActionFilter\HttpMethod::METHOD_POST,
                        ]
                    ),
                    //new ActionFilter\Csrf(),
                ],
                'postfilters' => []
            ]
        ];
    }
    public function getListAction(int $limit = null, int $offset = null): array
    {
        Loader::includeModule("iblock");

        $reviews = ElementReviewsTable::getList([
            "select" => [
                "RATING.VALUE",
                "DETAIL_TEXT",
                "BOOK.ELEMENT",
                "INNER_SECTION.NAME",
                "INNER_SECTION_PROP",
            ],
            "filter" => [],
            "runtime" => [
                'INNER_SECTION' => [
                    'data_type' => \Bitrix\Iblock\ElementTable::class,
                    'reference' => [
                        '=this.BOOK.VALUE' => 'ref.ID'
                    ],
                    'join_type' => 'LEFT'
                ],
            ],
            'count_total' => true,
            'limit'   => $limit,
            'offset'  => $offset
        ]);

        $res = $reviews->fetchAll()[0];
        //$res["all_count"] = $reviews->getCount();

        return $res;
    }
}
