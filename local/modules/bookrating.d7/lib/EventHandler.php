<?php

namespace Bookrating\Main;

class EventHandler
{
    function bookRatingCalculate(&$arFields): bool
    {
        \CModule::IncludeModule("iblock");

        $arSelect = array("ID", "NAME", "CODE", "PROPERTY_BOOK", "XML_ID");
        $arFilter = array("IBLOCK_ID" => $arFields["IBLOCK_ID"], "ID" => $arFields["XML_ID"], "ACTIVE" => "Y");

        $res = \CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter, false, false, $arSelect);

        $ob = $res->GetNextElement();

        $fields = $ob->GetFields();

        $bookId = $fields["PROPERTY_BOOK_VALUE"];

        $arSelect = array("ID", "NAME", "CODE", "PROPERTY_RATING");
        $arFilter = array("IBLOCK_CODE" => "reviews", "PROPERTY_BOOK" => $bookId, "ACTIVE" => "Y");

        $res = \CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter, false, false, $arSelect);

        $rating = 0;
        $count = 0;

        while ($ob = $res->GetNextElement()) {
            $fields = $ob->GetFields();
            $rating += (int)$fields["PROPERTY_RATING_VALUE"];
            $count++;
            file_put_contents("debug.log", print_r($fields, 1) . PHP_EOL, FILE_APPEND);
        }

        if ($count) {
            $rating = (int)($rating / $count);
            \CIBlockElement::SetPropertyValueCode($bookId, "RATING", $rating);
        }

        return true;
    }
}
