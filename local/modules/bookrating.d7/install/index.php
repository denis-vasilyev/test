<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Application;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\Loader;
use Bitrix\Main\Engine\CurrentUser;
use Bitrix\Main\Security\Random;

Loc::loadMessages(__FILE__);

class Bookrating_D7 extends CModule
{
    public  $MODULE_ID;
    public  $MODULE_VERSION;
    public  $MODULE_VERSION_DATE;
    public  $MODULE_NAME;
    public  $MODULE_DESCRIPTION;
    public  $PARTNER_NAME;
    public  $PARTNER_URI;
    public  $errors;

    function __construct()
    {
        // создаем пустой массив для файла version.php
        $arModuleVersion = array();
        // подключаем файл version.php
        include_once(__DIR__ . '/version.php');

        // версия модуля
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        // дата релиза версии модуля
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        // id модуля
        $this->MODULE_ID = "bookrating.d7";
        // название модуля
        $this->MODULE_NAME = "Отзывы и оценки на книги";
        // описание модуля
        $this->MODULE_DESCRIPTION = "Модуль с функционалом отзывов и оценок на книги";
        // имя партнера выпустившего модуль
        $this->PARTNER_NAME = "Денис Васильев";
        // ссылка на ресурс партнера выпустившего модуль
        $this->PARTNER_URI = "";
    }

    function DoInstall(): bool
    {
        global $APPLICATION;

        if(!$this->InstallIblocks()) return false;

        //$this->InstallDB();

        $this->InstallEvents();

        //$this->InstallFiles();

        ModuleManager::RegisterModule($this->MODULE_ID);

        $APPLICATION->includeAdminFile(
            Loc::getMessage('INSTALL_TITLE'),
            __DIR__ . '/installInfo.php'
        );

        return true;
    }

    function DoUninstall(): bool
    {
        global $APPLICATION;

        $this->UnInstallEvents();

        $this->UnInstallIblocks();

        ModuleManager::UnRegisterModule($this->MODULE_ID);

        $APPLICATION->includeAdminFile(
            Loc::getMessage('UNINSTALL_TITLE'),
            __DIR__ . '/unInstallInfo.php'
        );

        return true;
    }

    function InstallIblocks(): bool
    {
        if (!($ibBooksType = $this->CreateBooksIBlockType()) ||
            !($booksIBlockId = $this->CreateBooksIBlock($ibBooksType)) ||
            !($this->FillBooksIBlock($booksIBlockId))) {
            return false;
        }

        if (!($ibReviewsType = $this->CreateReviewsIBlockType()) ||
            !($reviewsIBlockId = $this->CreateReviewsIBlock($ibReviewsType, $booksIBlockId)) ||
            !($this->FillReviewsIBlock($reviewsIBlockId, $booksIBlockId))) {
            return false;
        }

        return true;
    }

    private function CreateBooksIBlockType(): ?string
    {
        global $DB;
        $arFields = array(
            "ID" => "books",
            "SECTIONS" => "N",
            "IN_RSS" => "N",
            "SORT" => 2,
            "LANG" => array(
                "ru" => array(
                    "NAME" => "Книги",
                    "SECTION_NAME" => "Категория",
                    "ELEMENT_NAME" => "Книга"
                ),
                "en" => array(
                    "NAME" => "Books",
                    "SECTION_NAME" => "Category",
                    "ELEMENT_NAME" => "Book"
                )
            )
        );

        $obCitiesIBlockType = new CIBlockType();

        $res = $obCitiesIBlockType->Add($arFields);

        if (!$res) {
            $DB->Rollback();
            return null;
        }

        $DB->Commit();

        return $arFields['ID'];
    }

    private function CreateBooksIBlock(string $iBlockType): ?int
    {
        $ib = new CIBlock();

        $siteId = "s1"; // ID сайта

        // Настройка доступа
        $arAccess = array(
            "2" => "R", // Все пользователи
        );

        $arFields = array(
            "ACTIVE" => "Y",
            "NAME" => "Книги",
            "CODE" => $iBlockType,
            "API_CODE" => $iBlockType,
            "REST_ON" => "Y",
            "IBLOCK_TYPE_ID" => $iBlockType,
            "SITE_ID" => $siteId,
            "SORT" => "5",
            "GROUP_ID" => $arAccess, // Права доступа
            "FIELDS" => array(
                // Символьный код элементов
                "CODE" => array(
                    "IS_REQUIRED" => "Y", // Обязательное
                    "DEFAULT_VALUE" => array(
                        "UNIQUE" => "Y", // Проверять на уникальность
                        "TRANSLITERATION" => "Y", // Транслитерировать
                        "TRANS_LEN" => "30", // Максимальная длина транслитерации
                        "TRANS_CASE" => "L", // Приводить к нижнему регистру
                        "TRANS_SPACE" => "-", // Символы для замены
                        "TRANS_OTHER" => "-",
                        "TRANS_EAT" => "Y",
                        "USE_GOOGLE" => "N",
                    ),
                ),
                // Символьный код разделов
                "SECTION_CODE" => array(
                    "IS_REQUIRED" => "Y",
                    "DEFAULT_VALUE" => array(
                        "UNIQUE" => "Y",
                        "TRANSLITERATION" => "Y",
                        "TRANS_LEN" => "30",
                        "TRANS_CASE" => "L",
                        "TRANS_SPACE" => "-",
                        "TRANS_OTHER" => "-",
                        "TRANS_EAT" => "Y",
                        "USE_GOOGLE" => "N",
                    ),
                ),
                "DETAIL_TEXT_TYPE" => array(      // Тип детального описания
                    "DEFAULT_VALUE" => "html",
                ),
                "SECTION_DESCRIPTION_TYPE" => array(
                    "DEFAULT_VALUE" => "html",
                ),
                "IBLOCK_SECTION" => array(         // Привязка к разделам обязательна
                    "IS_REQUIRED" => "N",
                ),
                "LOG_SECTION_ADD" => array("IS_REQUIRED" => "Y"), // Журналирование
                "LOG_SECTION_EDIT" => array("IS_REQUIRED" => "Y"),
                "LOG_SECTION_DELETE" => array("IS_REQUIRED" => "Y"),
                "LOG_ELEMENT_ADD" => array("IS_REQUIRED" => "Y"),
                "LOG_ELEMENT_EDIT" => array("IS_REQUIRED" => "Y"),
                "LOG_ELEMENT_DELETE" => array("IS_REQUIRED" => "Y"),

            ),

            // Шаблоны страниц
            "LIST_PAGE_URL" => "#SITE_DIR#/$iBlockType/",
            "SECTION_PAGE_URL" => "#SITE_DIR#/$iBlockType/#SECTION_CODE#/",
            "DETAIL_PAGE_URL" => "#SITE_DIR#/$iBlockType/#SECTION_CODE#/#ELEMENT_CODE#/",

            "INDEX_SECTION" => "Y", // Индексировать разделы для модуля поиска
            "INDEX_ELEMENT" => "Y", // Индексировать элементы для модуля поиска

            "VERSION" => 1, // Хранение элементов в общей таблице

            "ELEMENT_NAME" => "Книга",
            "ELEMENTS_NAME" => "Книги",
            "ELEMENT_ADD" => "Добавить книгу",
            "ELEMENT_EDIT" => "Изменить книгу",
            "ELEMENT_DELETE" => "Удалить книгу",
            "SECTION_NAME" => "Категория",
            "SECTIONS_NAME" => "Категории",
            "SECTION_ADD" => "Добавить категорию",
            "SECTION_EDIT" => "Изменить категорию",
            "SECTION_DELETE" => "Удалить категорию",

            "SECTION_PROPERTY" => "N", // Разделы каталога имеют свои свойства (нужно для модуля интернет-магазина)
        );

        $id = $ib->Add($arFields);

        if (!$id) {
            return null;
        }

        $ibp = new CIBlockProperty();

        $arFields = array(
            "NAME" => "Автор",
            "ACTIVE" => "Y",
            "SORT" => 100, // Сортировка
            "CODE" => "AUTHOR",
            "PROPERTY_TYPE" => "S", // Строка
            "IS_REQUIRED" => "Y",
            "FILTRABLE" => "Y", // Выводить на странице списка элементов поле для фильтрации по этому свойству
            "IBLOCK_ID" => $id
        );

        $propId = $ibp->Add($arFields);

        if (!$propId) {
            return null;
        }

        $ibp = new CIBlockProperty();

        $arFields = array(
            "NAME" => "Год",
            "ACTIVE" => "Y",
            "SORT" => 200, // Сортировка
            "CODE" => "YEAR",
            "PROPERTY_TYPE" => "N", // Число
            "IS_REQUIRED" => "Y",
            "FILTRABLE" => "Y", // Выводить на странице списка элементов поле для фильтрации по этому свойству
            "DEFAULT_VALUE" => 2000,
            "IBLOCK_ID" => $id
        );

        $propId = $ibp->Add($arFields);

        if (!$propId) {
            return null;
        }

        $ibp = new CIBlockProperty();

        $arFields = array(
            "NAME" => "Средняя оценка",
            "ACTIVE" => "Y",
            "SORT" => 300, // Сортировка
            "CODE" => "RATING",
            "PROPERTY_TYPE" => "N", // Число
            "IS_REQUIRED" => "N",
            "FILTRABLE" => "Y", // Выводить на странице списка элементов поле для фильтрации по этому свойству
            "DEFAULT_VALUE" => null,
            "IBLOCK_ID" => $id
        );

        $propId = $ibp->Add($arFields);

        if (!$propId) {
            return null;
        }

        return $id;
    }

    private function FillBooksIBlock($iBlockId): bool
    {
        $books = array(
            array("title" => "Книга1", "author" => "Автор1", "year" => "2005"),
            array("title" => "Книга2", "author" => "Автор2", "year" => "2010"),
            array("title" => "Книга3", "author" => "Автор3", "year" => "2015"),
            array("title" => "Книга4", "author" => "Автор4", "year" => "2020"),
        );

        foreach ($books as $book) {

            $el = new CIBlockElement();

            $prop = ["RATING" => rand(1, 100), "YEAR" => $book["year"] , "AUTHOR" => $book["author"]];

            $arElement = Array(
                "MODIFIED_BY" => CurrentUser::get()->getId(),
                "IBLOCK_ID" => $iBlockId,
                "NAME" => $book["title"],
                "CODE" => CUtil::translit($book["title"], "ru"),
                "PROPERTY_VALUES"=> $prop,
                "ACTIVE" => "Y"
            );

            if(!$el->Add($arElement)) {
                return false;
            }
        }

        return true;
    }

    private function CreateReviewsIBlockType(): ?string
    {
        global $DB;

        $arFields = Array(
            "ID"=>"reviews",
            "SECTIONS"=>"N",
            "IN_RSS"=>"N",
            "SORT"=>1,
            "LANG" => array(
                "ru" => array(
                    "NAME" => "Отзывы",
                    "SECTION_NAME" => "Отзывы",
                    "ELEMENT_NAME" => "Отзыв"
                ),
                "en" => array(
                    "NAME" => "Reviews",
                    "SECTION_NAME" => "Reviews",
                    "ELEMENT_NAME" => "Review"
                )
            )
        );

        $iBlockType = new CIBlockType();

        $DB->StartTransaction();

        $res = $iBlockType->Add($arFields);

        if(!$res)
        {
            $DB->Rollback();
            return false;
        }

        $DB->Commit();

        return $arFields['ID'];
    }

    private function CreateReviewsIBlock($iBlockType, $bookIBlockId): ?int
    {
        $ib = new CIBlock();

        $siteId = "s1"; // ID сайта

        // Настройка доступа
        $arAccess = array(
            "2" => "R", // Все пользователи
        );

        $arFields = array(
            "ACTIVE" => "Y",
            "NAME" => "Отзывы",
            "CODE" => $iBlockType,
            "API_CODE" => $iBlockType,
            "REST_ON" => "Y",
            "IBLOCK_TYPE_ID" => $iBlockType,
            "SITE_ID" => $siteId,
            "SORT" => "5",
            "GROUP_ID" => $arAccess, // Права доступа
            "FIELDS" => array(
                "DETAIL_TEXT_TYPE" => array(      // Тип детального описания
                    "DEFAULT_VALUE" => "html",
                ),
                "SECTION_DESCRIPTION_TYPE" => array(
                    "DEFAULT_VALUE" => "html",
                ),
                "IBLOCK_SECTION" => array(         // Привязка к разделам обязательна
                    "IS_REQUIRED" => "N",
                ),
                "LOG_ELEMENT_ADD" => array("IS_REQUIRED" => "Y"),
                "LOG_ELEMENT_EDIT" => array("IS_REQUIRED" => "Y"),
                "LOG_ELEMENT_DELETE" => array("IS_REQUIRED" => "Y"),
            ),

            // Шаблоны страниц
            "LIST_PAGE_URL" => "#SITE_DIR#/$iBlockType/",
            "SECTION_PAGE_URL" => "#SITE_DIR#/$iBlockType/#SECTION_CODE#/",
            "DETAIL_PAGE_URL" => "#SITE_DIR#/$iBlockType/#SECTION_CODE#/#ELEMENT_CODE#/",

            "INDEX_SECTION" => "Y", // Индексировать разделы для модуля поиска
            "INDEX_ELEMENT" => "Y", // Индексировать элементы для модуля поиска

            "VERSION" => 1, // Хранение элементов в общей таблице

            "ELEMENT_NAME" => "Обзор",
            "ELEMENTS_NAME" => "Обзоры",
            "ELEMENT_ADD" => "Добавить обзор",
            "ELEMENT_EDIT" => "Изменить обзор",
            "ELEMENT_DELETE" => "Удалить обзор",

            "SECTION_PROPERTY" => "N", // Разделы каталога имеют свои свойства (нужно для модуля интернет-магазина)
        );

        $id = $ib->Add($arFields);

        if (!$id) {
            return null;
        }

        $ibp = new CIBlockProperty();

        $arFields = array(
            "NAME" => "Оценка",
            "ACTIVE" => "Y",
            "SORT" => 100,
            "CODE" => "RATING",
            "PROPERTY_TYPE" => "N", // Число
            "IS_REQUIRED" => "Y",
            "FILTRABLE" => "Y", // Выводить на странице списка элементов поле для фильтрации по этому свойству
            "DEFAULT_VALUE" => 0,
            "IBLOCK_ID" => $id
        );

        $propId = $ibp->Add($arFields);
        if (!$propId) {
            return null;
        }

        $arFields = array(
            "NAME" => "Книга",
            "ACTIVE" => "Y",
            "SORT" => 200, // Сортировка
            "CODE" => "BOOK",
            "PROPERTY_TYPE" => "E", // Привязка к элементу инфоблока
            "IS_REQUIRED" => "Y",
            "FILTRABLE" => "Y", // Выводить на странице списка элементов поле для фильтрации по этому свойству
            "IBLOCK_ID" => $id,
            "LINK_IBLOCK_ID" => $bookIBlockId,
        );
        $propId = $ibp->Add($arFields);
        if (!$propId) {
            return null;
        }

        return $id;

    }

    private function FillReviewsIBlock($iBlockId, $bookIBlockId): bool
    {
        $arSelect = array("ID", "NAME", "CODE");
        $arFilter = array("IBLOCK_ID" => $bookIBlockId, "ACTIVE" => "Y");

        $res = CIBlockElement::GetList(array("SORT"=>"ASC"), $arFilter, false, false, $arSelect);

        $el = new CIBlockElement();

        $i = 0;

        while($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();

            $prop = ["RATING" => rand(1, 100), "BOOK" => array("n0" => $arFields["ID"])];

            $reviewsCount = rand(3, 6);

            for ($j = 0; $j < $reviewsCount; $j++) {
                $name = "Обзор - " . ++$i;
                $text = InsertSpaces(Random::getString(rand(50, 70), true), rand(5, 10));

                $arElement = array(
                    "MODIFIED_BY" => CurrentUser::get()->getId(),
                    "IBLOCK_ID" => $iBlockId,
                    "NAME" => $name,
                    "CODE" => CUtil::translit($name, "ru"),
                    "PREVIEW_TEXT" => $text,
                    "DETAIL_TEXT" => $text,
                    "PROPERTY_VALUES" => $prop,
                    "ACTIVE" => "Y"
                );

                if (!$el->Add($arElement)) {
                    return false;
                }
            }
        }

        return true;
    }

    function UnInstallIblocks(): bool
    {
        global $DB;

        $DB->StartTransaction();

        $reviewsIblockType = "reviews";

        if(!CIBlockType::Delete($reviewsIblockType))
        {
            $DB->Rollback();
            return false;
        }

        $booksIblockType = "books";

        if(!CIBlockType::Delete($booksIblockType))
        {
            $DB->Rollback();
            return false;
        }

        $DB->Commit();

        return true;
    }

    private function getIBlockIdByCode($code)
    {
        $res = CIBlock::GetList(
            array(),
            array(
                "CODE" => $code,
                "SITE_ID" => "s1"
            ), true
        );

        return $res->Fetch("ID");
    }

    function InstallEvents(): bool
    {
        RegisterModuleDependences(
            "iblock",
            "OnAfterIBlockElementUpdate",
            $this->MODULE_ID,
            "\\Bookrating\\Main\\EventHandler",
            "bookRatingCalculate"
        );
        RegisterModuleDependences(
            "iblock",
            "OnAfterIBlockElementAdd",
            $this->MODULE_ID,
            "\\Bookrating\\Main\\EventHandler",
            "bookRatingCalculate"
        );
        RegisterModuleDependences(
            "iblock",
            "OnAfterIBlockElementDelete",
            $this->MODULE_ID,
            "\\Bookrating\\Main\\EventHandler",
            "bookRatingCalculate"
        );

        return true;
    }

    function UnInstallEvents(): bool
    {
        UnRegisterModuleDependences(
            "iblock",
            "OnAfterIBlockElementUpdate",
            $this->MODULE_ID,
            "\\Bookrating\\Main\\EventHandler",
            "bookRatingCalculate"
        );
        UnRegisterModuleDependences(
            "iblock",
            "OnAfterIBlockElementAdd",
            $this->MODULE_ID,
            "\\Bookrating\\Main\\EventHandler",
            "bookRatingCalculate"
        );
        UnRegisterModuleDependences(
            "iblock",
            "OnAfterIBlockElementDelete",
            $this->MODULE_ID,
            "\\Bookrating\\Main\\EventHandler",
            "bookRatingCalculate"
        );

        return true;
    }
}
