<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$aMenu = array(
    // основная ветка меню
    array(
        // пункт меню в разделе Контент
        'parent_menu' => 'global_menu_services',
        // сортировка
        'sort' => 1,
        // название пункта меню
        'text' => "Модули Демо",
        // идентификатор ветви
        "items_id" => "menu_webforms",
        // иконка
        "icon" => "form_menu_icon",
        // дочерняя ветка меню
        'items' => array(
            array(
                // название подпункта меню
                'text' => 'Тестовый модуль',
                // ссылка для перехода
                'url' => 'settings.php?lang=ru&mid=bookrating.d7',
            ),
        )
    ),
);
// возвращаем основной массив $aMenu
return $aMenu;
