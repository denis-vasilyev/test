<?php

return [
    'controllers' => [
        'value' => [
            'defaultNamespace' => '\\Bookrating\\D7\\Controller',
            'namespaces' => [
                // Ключ - неймспейс для ajax-классов,
                // api - приставка экшенов, о ней мы поговорим чуть позже
                '\\Bookrating\\D7\\Controller' => 'api',
            ],
        ],
        'readonly' => true,
    ],
];